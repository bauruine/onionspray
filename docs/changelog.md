# Onionspray ChangeLog

## v1.6.0 - unreleased

* `./onionspray` command now outputs the `tor` daemon processes (commit
  [b130bcc80297f3625fe7d291abf81ed9990ec4ea][]).

[b130bcc80297f3625fe7d291abf81ed9990ec4ea]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/commit/b130bcc80297f3625fe7d291abf81ed9990ec4ea

#### Breaking changes

##### EOTK forked and renamed to Onionspray

Onionspray project starts as a fork of the [Enterprise Onion Toolkit (EOTK)][].
Details of the forking and renaming process can be found in these tickets:

* [tpo/onion-services/onionspray#1][].
* [tpo/onion-services/onionspray#13][].

Compatibility with [EOTK][] is kept: configuration, keys and certificates can
be easily [migrated](migration.md) from [EOTK][], and the `onionspray` command
it compatible with the `eotk` script (which is kept as an alias).

A migration procedure is available in [this tutorial](migration.md).

[EOTK]: https://github.com/alecmuffett/eotk
[Enterprise Onion Toolkit (EOTK)]: https://github.com/alecmuffett/eotk
[tpo/onion-services/onionspray#1]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/1
[tpo/onion-services/onionspray#13]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/13

##### HTTPS support (from 2022-03 onwards)

There was a small breaking change in order to better-support HARICA
as a certificate provider, but also for better usability; this
change impacts any project with a multi-onion EV certificate from
Digicert.

* v3 onion addresses used in pathnames are now truncated at 20 chars
  of onion, rather than 30 overall, to make shorter pathnames for unix
  domain sockets
* onion scratch-directory name changes:
    * was: `projects/tweep.d/abcdefghijklmnopqrstuvwxyza-v3.d/port-80.sock`
    * now: `projects/tweep.d/abcdefghijklmnopqrst-v3.d/port-80.sock`
    * this means that some scratch directories may be are remade,
    so a full restart is advisable after updating
* https certificate path-name changes
    * was: HTTPS certificate files used the full onion address
    * now: onion HTTPS certificates are now expected to be installed in
    per-onion-truncated-at-20 pathnames: e.g. for each ONIONADDRESS in
    PROJECTNAME:
        * `/projects/PROJECTNAME.d/ssl.d/ONIONADDRFIRST20CHAR-v3.onion.cert`
        * `/projects/PROJECTNAME.d/ssl.d/ONIONADDRFIRST20CHAR-v3.onion.pem`
    * this means that you will need to rename pre-existing certificate
    `cert` and `pem` files after you update and reconfigure;
    * **if you fail to do this you will experience "self-signed certificate" warnings**
* if you are using 'multi' certificates (such as some Digicert EV) where a
  single certificate contains all SubjectAltNames for 2+ onion
  addresses that are part of a single project:
    * do `set ssl_cert_each_onion 0` in the configuration, to re-enable
    multi cert handling
    * the names of the certificate files must be changed:
        * was: filenames used to be
        `projects/PROJECTNAME.d/ssl.d/PRIMARYONIONADDRESSWASHERE.{pem,cert}`
        * now: multi-certificates now must be named with the more meaningful
        `projects/PROJECTNAME.d/ssl.d/PROJECTNAME.{pem,cert}`

If you have any issues, please reach out to @alecmuffett on Twitter, or log an
issue above.

## v1.5.0 - 2021-05-18

* new features
* `eotk-site.conf`
  * "global" configuration rules to prefix into every configuration
  * auto-created it if it does not exist
  * used it to solve NGINX installation on Ubuntu 18.04LTS
* OnionBalance to be deprecated until overhauled by Tor
  * see notes in [HOW-TO-INSTALL.md](HOW-TO-INSTALL.md)
* Ubuntu 18.04LTS becomes mainline for Ubuntu
  * Ubuntu 16.04LTS becomes deprecated
* Support for v3 Onion Addresses
  * use `%NEW_V3_ONION%` in template configs (`.tconf`) to autogenerate
  * see `examples/wikipedia-v3.tconf` for examples

## v1.4.0 - 2019-06-27

* new features
  * auto-generate (`eotk make-scripts`) wrapper scripts for:
    * installation into "init" startup `eotk-init.sh`
    * log rotation / housekeeping via installation into eotk-user cronjob (`eotk-housekeeping.sh`)
  * stricter enforcement of onionification
    * by default will drop compressed, non-onionifiable content with error code `520`
    * if this is a problem (why?) use `set drop_unrewritable_content 0` to disable
  * validate worker onion addresses upon creation
    * works around [issue logged with tor](https://trac.torproject.org/projects/tor/ticket/29429)
  * `eotk spotpush` now permits pushing explicitly named scripts from `$EOTK_HOME`, as well as hunting in `projects`
  * `set hard_mode 1` is now **default** and onionifies both `foo.com` AND `foo\.com` (regexp-style)
    * use `set hard_mode 2` to further onionify `foo\\.com` and `foo\\\.com` at slight performance cost
  * `set ssl_mkcert 1` to use [mkcert](https://github.com/FiloSottile/mkcert) by @FiloSottile for certificate generation, if you have installed it
  * refactor nonces used in rewriting preserved strings
  * improvements to `set debug_trap pattern [...]` logging
  * support for OpenResty LuaJIT in Raspbian build scripts
  * update code version for Raspbian builds scripts
  * tor HiddenServiceVersion nits
  * dead code removal
  * deprecate support for Raspbian Jessie

## v1.3.0 - 2017-04-14

* new features
  * "Runbook" has been [moved to the documentation directory](docs/RUNBOOK.md)
  * tor2web has been blocked-by-default
    * since the reason for EOTK is to provide Clearnet websites with an Onion presence, Tor2web is not necessary
  * the `FORCE_HTTPS` feature has been added and made *default*
    * if your site is 100% HTTPS then you do not need to do anything,
    * however sites which require insecure `HTTP` may have to use `set force_https 0` in configurations.

## v1.2.0 - 2017-02-04?

* new features:
  * optional blocks to methods other than GET/HEAD
  * optional 403/Forbidden blocks for accesses to certain Locations or Hosts, including as regexps
    * nb: all blocks/block-patterns are *global* and apply to all servers in a project
  * optional time-based caching of static content for `N` seconds, with selectable cache size (def: 16Mb)
* new [How To Install](docs/HOW-TO-INSTALL.md) guide
* custom install process for Ubuntu, tested on Ubuntu Server 16.04.2-LTS
* renaming / factor-out of Raspbian install code
* fixes to onionbalance support

## v1.1.0 - 2017-04-04

* first cut of onionbalance / softmap

## v1.0.0 - 2017-02-12

* have declared a stable alpha release
* architecture images, at bottom of this page
* all of CSP, HSTS and HPKP are suppressed by default; onion networking mitigates much of this
* ["tunables"](docs/TEMPLATES.md) documentation for template content
* `troubleshooting` section near the bottom of this page
* See [project activity](https://github.com/alecmuffett/eotk/graphs/commit-activity) for information
