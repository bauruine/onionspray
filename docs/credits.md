# Acknowledgements

Onionspray is a fork from [EOTK][] and stands largely on the experience of work
Alec Muffett led at Facebook to create
`www.facebookcorewwwi.onion`[^facebook-onion], but it owes a *huge* debt to
[Mike Tigas][]'s work at ProPublica to put their site into Onionspace through
using NGINX as a rewriting proxy -- and that [he wrote the whole experience up
in great detail][] including [sample config files][].

Reading this prodded me to learn about NGINX and then aim to shrink &
genericise the solution; so thanks, Mike!

Also, thanks go to Christopher Weatherhead for acting as a local NGINX
*sounding board* :-)

And back in history: Michal Nánási, Matt Jones, Trevor Pottinger and
the rest of the FB-over-Tor team. Hugs.

[EOTK]: https://github.com/alecmuffett/eotk
[Mike Tigas]: https://github.com/mtigas
[he wrote the whole experience up in great detail]: https://www.propublica.org/nerds/item/a-more-secure-and-anonymous-propublica-using-tor-hidden-services
[sample config files]: https://gist.github.com/mtigas/9a7425dfdacda15790b2

[^facebook-onion]: The current Facebook Onion Service v3 is `facebookwkhpilnemxj7asaniu7vnjjbiltxjqhye3mhbshg7kx5tfyd.onion`.

## Notes
