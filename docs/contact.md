# Contacts, requests and bug reporting

## Contacting

* [Onion Services section][] at the [Tor Forum][].
* Mailing list: [tor-dev@lists.torproject.org][].
* [IRC][]: #tor-dev at [OFTC][].

[Onion Services section]: https://forum.torproject.org/c/support/onion-services/16
[Tor Forum]: https://forum.torproject.org
[IRC]: https://en.wikipedia.org/wiki/Internet_Relay_Chat
[OFTC]: https://www.oftc.net/
[tor-dev@lists.torproject.org]: https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-dev

## Reporting bugs and features

Report bugs, request features at the [Onionspray issue queue][], and submit
merge requests [here][].

[Onionspray issue queue]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues
[here]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/merge_requests
