# Command List

## Flags

* `--local`: ignore the presence of `onionspray-workers.conf` and operate
  upon local projects; used to administer projects running locally on
  a machine which might *also* be running onionbalance.
* `--remote`: functionally the same as `--local` but denotes remote
  execution on a worker; used to inhibit recursion and loops amongst
  worker machines, of A calls B calls A calls B...

## Configuration

* `onionspray config [filename]` # default `onions.conf`
    * *synonyms:* `conf`, `configure`
    * parses the config file and sets up and populates the projects
* `onionspray maps projectname ...` # or: `-a` for all
    * prints which onions correspond to which dns domains
    * for softmap, this list may not show until after `ob-config` and `ob-start`
* `onionspray harvest projectname ...` # or: `-a` for all
    * *synonyms:* `onions`
    * prints list of onions used by projects

## Onion Generation

* `onionspray genkey`
    * *synonyms:* `gen`
    * generate an onion key and stash it in `secrets`

## Project Status & Debugging

* `onionspray status projectname ...` # or: `-a` for all
    * active per-project status
* `onionspray ps`
    * do a basic grep for possibly-orphaned processes
* `onionspray debugon projectname ...` # or: `-a` for all
    * enable verbose tor logs
* `onionspray debugoff projectname ...` # or: `-a` for all
    * disable verbose tor logs

## Starting & Stopping Projects

* `onionspray start projectname ...` # or: `-a` for all
    * start projects
* `onionspray stop projectname ...` # or: `-a` for all
    * stop projects
* `onionspray restart projectname ...` # or: `-a` for all
    * *synonyms:* `bounce`, `reload`
    * stop, and restart, projects
* `onionspray nxreload projectname ...` # or: `-a` for all
    * politely ask NGINX to reload its config files

## Starting & Stopping Onionbalance

* `onionspray ob-start projectname ...` # or: `-a` for all, if applicable
    * *synonyms:*
* `onionspray ob-restart projectname ...` # or: `-a` for all, if applicable
    * *synonyms:*
* `onionspray ob-stop`
    * *synonyms:*
* `onionspray ob-status`
    * *synonyms:*

## Configuring Remote Workers

* `onionspray-workers.conf`
    * if not present, only `localhost` will be used
    * if present, contains one hostname per line, no comments
      * the label `localhost` is a hardcoded synonym for local activity
      * other (remote) systems are accessed via `ssh`, `scp` & `rsync`
* `onionspray ob-remote-nuke-and-push`
    * *synonyms:*
* `onionspray ob-nxpush`
    * *synonyms:*
* `onionspray ob-torpush`
    * *synonyms:*
* `onionspray ob-spotpush`
    * *synonyms:*

## Backing-Up Remote Workers

* onionspray `mirror`
    * *synonyms:*
* onionspray `backup`
    * *synonyms:*
