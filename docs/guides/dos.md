# Denial of Service (DoS) mitigations

Onionspray implements many of the [available Onion service DoS protections][] that
can be used alone or altogether.

[available Onion service DoS protections]: https://community.torproject.org/onion-services/advanced/dos/).

## Introduction Point-based defenses

To mitigate DoS attacks at the introduction points, use the following settings,
here provided with example values:

    set tor_intro_dos_defense 1
    set tor_intro_dos_burst_per_sec 200
    set tor_intro_dos_rate_per_sec 25

They work exactly as C Tor's `HiddenServiceEnableIntroDoSDefense`
`HiddenServiceEnableIntroDoSBurstPerSec` and
`HiddenServiceEnableIntroDoSRatePerSec`[^manpage].

## Proof of Work (PoW) protection

The following configuration options are available to enable and fine-tune PoW:

  set tor_pow_enabled 1
  set tor_pow_queue_rate 250
  set tor_pow_queue_burst 2500

They're analogous to C Tor's `HiddenServicePoWDefensesEnabled`,
`HiddenServicePoWQueueRate` and `HiddenServicePoWQueueBurst` configuration
options[^manpage].

Please tune them according to your needs.

## Stream-based defenses

Connections can be limited in the rendezvous streams by using these settings
(change the number according to your needs):

    set tor_max_streams 2000
    set tor_max_streams_close_circuit 1

They account, respectively, to C Tor's `HiddenServiceMaxStreams` and
`HiddenServiceMaxStreamsCloseCircuit`[^manpage].

## Load balancing

Check the [load balancing page](balance/README.md) for details.

## Webserver rate limiting

This is partially built through this setting equivalent to C Tor's
`HiddenServiceExportCircuitID`[^manpage]:

    set tor_export_circuit_id haproxy

When this is set to `haproxy`, the Onion Service circuit IDs will be available
for the proxy (and will show up in the logs), but we still proxy integration to
do full rate limiting, which is a task being tracked on ticket
[tpo/onion-services/onionspray#18][].

[tpo/onion-services/onionspray#18]: https://gitlab.torproject.org/tpo/onion-services/onionspray/-/issues/18

## Notes

[^manpage]: Details on these options are available at the `tor(1)` manual page.
